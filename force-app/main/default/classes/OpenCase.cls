public class OpenCase {       
	public String accid=ApexPages.currentPage().getParameters().get('accid');
	
	public String getAid(){
		return this.accid;
	}
	public List<Case> cases=new List<Case>();
	
	public List<Case> getCases(){
		return this.cases;
	}
	
	public OpenCase(){
		System.debug('-------------constuctor--------------------');
	}
	public pageReference init(){
		System.debug('-------------init--------------------');
		cases=[select id,Subject,CaseNumber,Status,ClosedDate,AccountId,CreatedDate 
				from Case where AccountId=:accid and Status='New'
				order by CreatedDate desc limit 999];
		return null;
	}
	
	public TestMethod static void test(){
		OpenCase ass=new OpenCase();  
		Account acc=new Account();
		acc.Name='test';
		insert acc;
		Case a=new Case();
		a.AccountId=acc.id;
		insert a;
		ass.accid=acc.id;
		ass.init();
		ass.getCases();
	}
}