public class ResolvedCase {
	public String accid=ApexPages.currentPage().getParameters().get('accid');
	
	public String getAid(){
		return this.accid;
	}
	public List<Case> cases=new List<Case>();
	
	public List<Case> getCases(){
		return this.cases;
	}
	
	public ResolvedCase(){
		System.debug('-------------constuctor--------------------');
	}
	public pageReference init(){
		String[] sts=new String[]{};
		Schema.DescribeFieldResult F=Case.Status.getDescribe();
	 	List<Schema.PicklistEntry> P=F.getPicklistValues();
	 	for(Schema.PicklistEntry  pe:P){
	 		if(pe.getValue().contains('Resolved')){
				sts.add(pe.getValue());
	 		}
	 	}
		System.debug('-------------init--------------------');
		cases=[select id,Subject,CaseNumber,Status,ClosedDate,AccountId,CreatedDate 
				from Case where AccountId=:accid and Status in:sts
				order by CreatedDate desc limit 999];
		return null;
	}
	
	public TestMethod static void test(){
		ResolvedCase ass=new ResolvedCase();  
		Account acc=new Account();
		acc.Name='test';
		insert acc;
		Case a=new Case();
		a.AccountId=acc.id;
		insert a;
		ass.accid=acc.id;
		ass.init();
		ass.getCases();
	}
}