public class FAQ {
	public String welmess{get;set;}
	public String accid=ApexPages.currentPage().getParameters().get('accid');
	
	public String getAid(){
		return this.accid;
	}
	public FAQ(){
		System.debug('-------------constuctor--------------------');
	}
	public pageReference init(){
		System.debug('-------------init--------------------');
		List<Website_Elements__c> wes=[select id,Name__c,Element_Content__c,page__c from Website_Elements__c 
			where page__c='FAQ' and Name__c='FAQ' order by CreatedDate];
		if(wes.size()>0){
			welmess=wes[0].Element_Content__c;
		}
		return null;               
	}
	public TestMethod static void testWe(){
		FAQ fs=new FAQ();
		fs.getAid();
		fs.init();
	}
}