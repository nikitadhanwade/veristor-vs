public class publicPage {
	public String aid;
	public String getAid(){
		return aid;
	}
	public String conName{get;set;}
	public Account acc{get;set;}
	public News_Media__c[] nms=new News_Media__c[]{};
	public News_Media__c[] getNms(){
		return this.nms;
	}
	public publicPage(){
		conName=userinfo.getName();
		nms=[select id,Name,Show_in_Portal__c,URL_to_Story__c from News_Media__c where Show_in_Portal__c=true limit 1000];
		List<User> us=[select id,AccountId from User where id=:userinfo.getUserId()];
		if(us!=null && us.size()>0){
			String accid=us[0].AccountId;
			if(accid!=null && accid!=''){
				List<Account> accs=[select a.of_Opportunities__c, a.Year_Business_Started__c, a.Website, 
						a.Type, a.Ticker_Symbol__c, a.TickerSymbol, a.SystemModstamp, 
						a.State_of_Sales_Use_Tax_Exemption_Cert__c, a.State_of_Incorporation__c, 
						a.State__c, a.State_ID__c, a.ShippingStreet, a.ShippingState, 
						a.ShippingPostalCode, a.ShippingCountry, a.ShippingCity, 
						a.Sales_and_Use_Tax_Exempt__c, a.Sales_Tax_Exemption_Certificate__c, 
						a.Phone, a.Personal_Guaranty_s_Driver_s_License__c, 
						a.Personal_Guaranty_Social_Security__c, 
						a.Personal_Guaranty_Phone_Number__c, 
						a.Personal_Guaranty_Name__c, a.Personal_Guaranty_Home_Address__c, 
						a.Personal_Guaranty_Available__c, a.Parent_Company__c, a.ParentId, a.OwnerId, 
						a.Name, a.MasterRecordId, a.Legal_Name__c, a.LastModifiedDate, 
						a.LastModifiedById, a.LastActivityDate, a.IsDeleted, a.IsCustomerPortal, 
						a.Id, a.Guarantor_s_Name__c, a.First_Call_Customer__c, a.Federal_ID__c, 
						a.Fax, a.FIscal_Year_End__c, a.Declined_Partial_First_Call_See_Assets__c, 
						a.Declined_1st_Call_Renewal__c, a.Date_of_Last_Financial_Statement__c, 
						a.D_B_Number__c, a.Credit_rating__c, a.CreatedDate, a.CreatedById, 
						a.Corporate_Headquarters_Address__c, a.Corporate_Guaranty_Available__c, 
						a.Corp_Type__c, a.Company_Type__c, a.BillingStreet, a.BillingState, 
						a.BillingPostalCode, a.BillingCountry, a.BillingCity, a.Bank_Name__c, 
						a.Bank_Contact_Phone__c, a.Bank_Contact_Name__c, a.Bank_Contact_Email_Address__c 
						from Account a where id=:accid];
				if(accs!=null && accs.size()>0){
					acc=accs[0];
					aid=acc.id;
				}
			}
		}
	}
	public static testmethod void testpub(){
		publicPage pu=new publicPage();
		
	}
}