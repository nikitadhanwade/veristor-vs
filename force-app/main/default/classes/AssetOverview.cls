public class AssetOverview {
	public String accid=ApexPages.currentPage().getParameters().get('accid');
	public List<Inna> assets=new List<Inna>();
	public boolean selectAll{get;set;}
	public String ids{get;set;}
	public String accName{get;set;}
	
	public List<Asset> assetslis=new List<Asset>();
	public List<Asset> getAssetslis(){
		return this.assetslis;
	}
	
	public String getAid(){
		return this.accid;
	}
	
	class Inna{
		public Asset ass{get;set;}
		public boolean flag{get;set;}
	}
	public List<Inna> getAssets(){
		return this.assets;
	}
	public void setAssets(List<Inna> inns){
		this.assets=inns;
	}
	
	public AssetOverview(){
		System.debug('-------------constuctor--------------------');
	}
	public pageReference init(){
		System.debug('-------------init--------------------');
		if(accid!=null){
			List<Asset> asse=[select id,Name,Man_Maint_End__c,AccountId,Description,Status,Product2Id,Product2.Name,Manufacturer_Name__c,
				Service_Tag__c,SerialNumber,FC_Status__c ,First_Call_Contr_End__c,Product_Info__c,Product_Info__r.Name,Account.Name
				from Asset where AccountId=:accid order by name desc limit 1000];
			if(asse!=null && asse.size()>0){
				accName=asse[0].Account.Name;
				for(Asset a:asse){
					Inna inn=new Inna();
					inn.flag=false;
					inn.ass=a;
					assets.add(inn);
				}
			}
		} 
		return null; 
	}
	public pageReference sel(){
		
		ids='';
		for(Inna inn:assets){
			if(inn.flag==true && ids.length()<1800){
//				ids+=String.valueOf(inn.ass.id).substring(0,15)+';';
				ids+=inn.ass.id+';';
			}
		}
		selectAll=true;
		System.debug(ids.length()+'--------------'+ids);
		return new pageReference('/PrintSelectedAssets?ids='+ids);
	}
	public TestMethod static void test(){
		AssetOverview ass=new AssetOverview();
		Account acc=new Account();
		acc.Name='test';
		insert acc;
		Asset a=new Asset();
		a.AccountId=acc.id;
		a.Name='testAsset';
		insert a;
		ass.accid=acc.id;
		ass.init();
		ass.getAssets();
		ass.selectAll=true;
		List<Inna> inns=new List<Inna>();
		ass.setAssets(inns);
		ass.sel();
	}
}