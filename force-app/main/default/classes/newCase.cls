public class newCase {
	public News_Media__c[] nms=new News_Media__c[]{};
	public String conName{get;set;}
	public News_Media__c[] getNms(){
		return this.nms;
	}
	public String aid;
	public String getAid(){
		return aid;
	}
	public boolean IsSucess{get;set;}
	public String casNum{get;set;}
	
	
	public blob docBody{get;set;}
	public String docName{get;set;}
	
	public String accid=ApexPages.currentPage().getParameters().get('accid');
	public String assid=ApexPages.currentPage().getParameters().get('assid');
	public Case c{get;set;}
	public String test{get;set;}
	public Inna con{get;set;}
	public String mess='';
	public String accName{get;set;}
	
	public List<SelectOption> Prioritys{get;set;}
	
	public List<SelectOption> assets=new List<SelectOption>();
	public List<SelectOption> cons=new List<SelectOption>();
	
	public map<Id,Asset> assmap=new map<Id,Asset>();
	public Asset[] assList=new Asset[]{};
	
	public List<SelectOption> typs=new List<SelectOption>();
	public List<SelectOption> reas=new List<SelectOption>();
	
	public List<SelectOption> getAssets(){
		return this.assets;
	}
	public List<SelectOption> getCons(){
		return this.cons;
	}
	public List<SelectOption> getTyps(){
		return this.typs;
	}
	public List<SelectOption> getReas(){
		return this.reas;
	}
	
	public String getMess(){
		return this.mess;
	}
	class Inna{
		public String LastName{get;set;}
		public String FirstName{get;set;}
		public String Title{get;set;}
		public String Email{get;set;}
		public String MobilePhone{get;set;}
		public String Phone{get;set;}
	}
	public newCase(){
		System.debug('-------------constuctor--------------------');
	}
	public pageReference init(){
		conName=userinfo.getName();
		System.debug('---------init----------');
		/*String High='High - Critical Business Impact'+' (Response within 1/2 hour) - Please call First Call Support @ 877-686-4375'
										+'\nProduction server or other mission critical system(s) are down and no workaround is immediately available.'
										+'You have had a substantial loss of service.'
										+'Your business operations have been severely disrupted.'
										+'High Severity support requires you to have dedicated resources available to work on the issue on an ongoing basis during your contractual hours.';
		String Medium='Medium - Significant Business Impact'+' (Response within 1 hour):'
										+'\nMajor functionality is severely impaired.'
										+'Operations can continue in a restricted fashion, although long-term productivity might be adversely affected.'
										+'Impaired operations';
		String Low='Low - Minimal Business Impact'+' (Response within 4 hours):'
										+'\nGeneral usage questions.'
										+'A temporary workaround is available.';
		Prioritys=new List<SelectOption>();
		Prioritys.add(new Selectoption('High',High));
		Prioritys.add(new Selectoption('Medium',Medium));
		Prioritys.add(new Selectoption('Low',Low));*/
		
		/*Prioritys=new List<SelectOption>();
		Prioritys.add(new Selectoption('High','High (Critical Business Impact)'));
		Prioritys.add(new Selectoption('Medium','Medium (Significant Business Impact)'));
		Prioritys.add(new Selectoption('Low','Low (Minimal Business Impact)'));*/
		
		typs.add(new Selectoption('Configuration Help','Configuration Help'));
		typs.add(new Selectoption('Hardware Issue','Hardware Issue'));
		typs.add(new Selectoption('Software Issue','Software Issue'));
		typs.add(new Selectoption('Information Request','Information Request'));
		typs.add(new Selectoption('Installation Assistance','Installation Assistance'));
		typs.add(new Selectoption('Documentation Request','Documentation Request'));
		typs.add(new Selectoption('Product Update Notification','Product Update Notification'));
		reas.add(new Selectoption('New Problem','New Problem'));
		reas.add(new Selectoption('Existing Problem','Existing Problem'));
		c=new Case();
		User u=[select id,Name,ContactId,Contact.Name from User where id=:userinfo.getUserId()];
		if(u!=null){
			c.ContactId=u.ContactId;
		}
		c.AccountId=accid;
		c.AssetId=assid;
		
		nms=[select id,Name,Show_in_Portal__c,URL_to_Story__c from News_Media__c where Show_in_Portal__c=true limit 1000];
		//List<User> us=[select id,AccountId from User where id=:userinfo.getUserId()];
		aid=accid;
		
		con=new Inna();
		Account[] accs=[select id,Name,(select id,Name from Contacts) from Account where id=:accid];
		cons.add(new Selectoption('003000000000000','--None--'));
		if(accs!=null && accs.size()>0){
			accName=accs[0].Name;
			if(accs[0].Contacts!=null && accs[0].Contacts.size()>0){
				for(Contact cont:accs[0].Contacts){
					cons.add(new Selectoption(cont.id,cont.Name));
                    if(cont.Name==userinfo.getName()){
                    	c.ContactId=cont.id;
                    }
				}
			}
		}
		cons.add(new Selectoption('003000000000001','--Create New Contact--'));
		/*assmap=new map<ID,Asset>([select id,Name,AccountId,Account.Name,Status,Product2Id,Description ,Product2.Name,
						FC_Status__c,Product_Info__c,Product_Info__r.Name ,SerialNumber,Service_Tag__c
						from Asset where AccountId=:accid order by Name limit 1000]);*/
		assList=[select id,Name,AccountId,Account.Name,Status,Product2Id,Description ,Product2.Name,
						FC_Status__c,Product_Info__c,Product_Info__r.Name ,SerialNumber,Service_Tag__c
						from Asset where AccountId=:accid order by Name limit 100];
		if(assList.size()>0){
			assmap=new map<ID,Asset>(assList);
		}
		assets.clear();
		assets.add(new Selectoption('02i400000000000000','--None--'));
		for(Asset a:assList){
			if(a.SerialNumber!=null){
				assets.add(new Selectoption(a.id,a.Name+' - '+a.SerialNumber));
			}else if(a.Service_Tag__c!=null){
				assets.add(new Selectoption(a.id,a.Name+' - '+a.Service_Tag__c));
			}else {
				assets.add(new Selectoption(a.id,a.Name));
			}
			if(a.id==assid){
				c.Product_Serial_Number__c=a.SerialNumber;
			}
		}
		
		System.debug('-------------init--------------------');
		List<Website_Elements__c> wes=[select id,Name__c,Element_Content__c,page__c from Website_Elements__c 
			where page__c='Portal New Case'];
		if(wes!=null && wes.size()>0){
			for(Website_Elements__c we:wes){
				if(we.Name__c=='Submit a New Case to Veristor'){
					mess=we.Element_Content__c;
				}
			}
		}
		
		return null;
	}
	public void selAsset(){
		System.debug('------------'+c.AssetId);
		if(assmap.containskey(c.AssetId)){
			if(assmap.get(c.AssetId).SerialNumber!=null){
				c.Product_Serial_Number__c=assmap.get(c.AssetId).SerialNumber;
			}else{
				c.Product_Serial_Number__c=assmap.get(c.AssetId).Service_Tag__c;
			}
		}else {
			c.Product_Serial_Number__c=null;
		}
	}
	public pageReference crecon(){
		System.debug('-------------crecon--------------------');
		System.debug('-------------1--------------------'+con.LastName);
		System.debug('-------------2--------------------'+con.FirstName);
		System.debug('-------------3--------------------'+con.Title);
		if(con.LastName!=null && con.LastName!='' && accid!=''){
			Contact contact=new Contact();
			contact.LastName=con.LastName;
			contact.FirstName=con.FirstName;
			contact.Title=con.Title;
			contact.Phone=con.Phone;
			contact.MobilePhone=con.MobilePhone;
			contact.Email=con.Email;
			contact.AccountId=accid;
			insert contact;
			String nam='';
			if(con.FirstName!=null && con.FirstName!=''){
				nam=con.FirstName;
			}
			nam=nam+' '+con.LastName;
			System.debug('-------------4--------------------'+nam);
			cons.add(new Selectoption(contact.id,nam));
			c.ContactId=contact.id;
		}
		return null;
	}
	public void save(){
		Savepoint sp=Database.setSavepoint();
		try{
			System.debug('======='+docName);
			
			System.debug('-------------save--------------------');
			if(c.AssetId=='02i400000000000000'){
				c.AssetId=null;
			}
			if(c.ContactId=='003000000000000'){
				c.ContactId=null;
			}
			System.debug('-------------test--------------------'+test);
			System.debug('-------------subject--------------------'+c.Subject);
			System.debug('-------------sdd--------------------'+c.AssetId);
			System.debug('-------------description--------------------'+c.Description);
			System.debug('-------------contact--------------------'+c.ContactId);
			if(c.Subject!=null && c.Subject!='' && c.AssetId!='02i400000000000000'){
				insert c;
				IsSucess=true;
				casNum=c.CaseNumber;
				if(docBody!=null && docName!=null){
					Attachment att=new Attachment();
					att.Body=docBody;
					att.Name=docName;
					att.ParentId=c.id;
					insert att;
				}
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				System.debug('-----userName--'+userinfo.getName());
				Contact[] cons=[select id,Name from Contact where Name=:userinfo.getName() and AccountId=:aid limit 1];
				if(cons.size()>0){
					mail.setTargetObjectId(cons[0].id);
					mail.setTemplateId('00X30000000dBsI');
					mail.setWhatId(c.id);    
					mail.setUseSignature(false);
					//mail.setReplyTo('fengrr@cloudcc.com');
					mail.setSaveAsActivity(false);  
					Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
				}
			}else{
				//Apexpages.addMessages(new apexpages.Message(ApexPages.Severity.FATAL,'please input the required field!'));
			}
		}catch(Exception ex){ 
			Database.rollback(sp);
			Apexpages.addMessages(ex);
		}
		//return new pageReference('/welcome');
	}
	public pageReference cancel(){
		System.debug('-------------cancel--------------------');
		return new pageReference('/welcome');
	}
	public pageReference upload(){
		return null;
	}
	public TestMethod static void test(){
		newCase ass=new newCase();
		Account acc=new Account();
		acc.Name='test';
		insert acc;
		Contact con=new Contact();
		con.LastName='test';
		con.AccountId=acc.id;
		insert con;
		Asset aset=new Asset();
		aset.AccountId=acc.id;
		aset.Name='testass';
		insert aset;
		Website_Elements__c we=new Website_Elements__c();
		we.Element_Content__c='etst';
		we.page__c='Portal New Case';
		we.Name__c='Submit a New Case to Veristor';
		insert we;
		
		ass.accid=acc.id;
		ass.init();
		ass.getAid();
		ass.getAssets();
		ass.getCons();
		ass.getMess();
		ass.getNms();
		ass.getReas();
		ass.getTyps();
		ass.con.LastName='test';
		ass.con.FirstName='test';
		ass.crecon();
		ass.docName='testdoc';
		ass.docBody=blob.valueOf('testestetsetsetsetsetset');
		ass.save();
		ass.cancel();
	}
}