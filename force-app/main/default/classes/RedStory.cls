public with sharing class RedStory {
	public String id=ApexPages.currentPage().getParameters().get('id');
	public News_Media__c nm{get;set;}
	
	
	public PageReference init(){
		News_Media__c[] nms=[select id,Name,Show_in_Portal__c,URL_to_Story__c ,Content__c from News_Media__c where id=:id];
		if(nms!=null && nms.size()>0){
			nm=nms[0];
		}
		return null;
	}
	public static testMethod void test(){
		RedStory r=new RedStory();
		News_Media__c[] nms=[select id,Name,Show_in_Portal__c,URL_to_Story__c,Content__c from News_Media__c limit 1];
		if(nms!=null && nms.size()>0){
			r.id=nms[0].id;
		}
		r.init();
	}
}