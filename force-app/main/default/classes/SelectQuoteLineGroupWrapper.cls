public class SelectQuoteLineGroupWrapper {

    public string listPrice;
    public string selectQuoteLineGroup;
    
    public SelectQuoteLineGroupWrapper(string listPrice,string selectQuoteLineGroup){
        this.listPrice = listPrice;
        this.selectQuoteLineGroup = selectQuoteLineGroup;           
    }
}