public class AssetDetail {
	public Asset a=new Asset();
	public String id=ApexPages.currentPage().getParameters().get('id');
	
	public String getAid(){
		return this.id;       
	}
	public Asset getA(){
		return this.a;
	}
	public AssetDetail(){
		System.debug('-------------constuctor--------------------');
	}
	public pageReference init(){
		System.debug('-------------init--------------------');
		List<Asset> assets=[select a.Vendor_Support_Type__c, a.VMware_Cust__c, a.UsageEndDate, 
				a.SystemModstamp, a.Status, a.Service_Tag__c, a.SerialNumber, a.Quantity, a.PurchaseDate, 
				a.Product_Info__c, a.Product2Id, a.Price, a.Name, a.Manufacturer_Name__c, a.Man__c, 
				a.Man_Maint_End__c,a.Id, a.First_Call_Contr_End__c, a.FC_Status__c, 
				a.Estimated_EOL__c, a.Description, a.CreatedDate, a.CreatedById, a.ContactId, 
				a.CommCell__c, a.Cisco_Contract__c, a.AccountId ,a.Product_Info__r.BakBone__c,
				a.Product_Info__r.BakBone_Documentation__c,a.Product_Info__r.BakBone_Firmware__c,a.Product_Info__r.name,
				a.logo__c,a.home__c,a.docume__c,a.Product_Info__r.Logo__c,a.Product_Info__r.Asset_Home_Page__c,
				a.Product_Info__r.Registration_Portal_Login_Page__c,a.Product_Info__r.Documentation_Friendly_Name__c,
				a.Product_Info__r.Documentation__c,a.Product_Info__r.Firmware__c,a.Product_Info__r.Asset_Friendly_Name__c,
				a.Product_Info__r.Firmware_Friendly_Name__c,a.Product_Info__r.Registration_Portal_Friendly_Name__c
				from Asset a where id=:id];
		if(assets!=null && assets.size()>0){
			a=assets[0];
			if(a.Product_Info__r.Asset_Home_Page__c!=null && a.Product_Info__r.Asset_Home_Page__c.contains('https://')){
				a.Product_Info__r.Asset_Home_Page__c=a.Product_Info__r.Asset_Home_Page__c.replace('https://','');
			}
			if(a.Product_Info__r.Registration_Portal_Login_Page__c!=null && a.Product_Info__r.Registration_Portal_Login_Page__c.contains('https://')){
				a.Product_Info__r.Registration_Portal_Login_Page__c=a.Product_Info__r.Registration_Portal_Login_Page__c.replace('https://','');
			}
			if(a.Product_Info__r.Documentation__c!=null && a.Product_Info__r.Documentation__c.contains('https://')){
				a.Product_Info__r.Documentation__c=a.Product_Info__r.Documentation__c.replace('https://','');
			}
			if(a.Product_Info__r.Firmware__c!=null && a.Product_Info__r.Firmware__c.contains('https://')){
				a.Product_Info__r.Firmware__c=a.Product_Info__r.Firmware__c.replace('https://','');
			}
		}
		return null;
	}
	public PageReference crepdf(){
		return new PageReference('/newCase?accid='+a.AccountId+'&assid='+a.id);
	}
	public TestMethod static void test(){
		Product_Information__c  pin=new Product_Information__c ();
		pin.Documentation__c='https://www.google.com';
		pin.Asset_Home_Page__c='https://www.google.com';
		pin.Registration_Portal_Login_Page__c='https://www.google.com';
		pin.Firmware__c='https://www.google.com';
		insert pin;
		Account acc=new Account();
		acc.Name='test';
		insert acc;
		Asset a=new Asset();
		a.AccountId=acc.id;
		a.Name='testAsset';
		a.Product_Info__c=pin.id;
		insert a;
		AssetDetail ass=new AssetDetail();
		ass.id=a.id;
		ass.init();
		ass.getA();
		ass.getAid();
		ass.crepdf();
	}
}