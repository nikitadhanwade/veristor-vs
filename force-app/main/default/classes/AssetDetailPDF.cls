public with sharing class AssetDetailPDF {
	public Asset a=new Asset();
	public String id=ApexPages.currentPage().getParameters().get('id');
	
	public String getAid(){
		return this.id;       
	}
	public Asset getA(){
		return this.a;
	}
	public pageReference init(){
		System.debug('-------------init--------------------');
		List<Asset> assets=[select a.Vendor_Support_Type__c, a.VMware_Cust__c, a.UsageEndDate, 
				a.SystemModstamp, a.Status, a.Service_Tag__c, a.SerialNumber, a.Quantity, a.PurchaseDate, 
				a.Product_Info__c, a.Product2Id, a.Price, a.Name, a.Manufacturer_Name__c, a.Man__c, 
				a.Man_Maint_End__c, a.LastModifiedDate, a.LastModifiedById, a.IsDeleted, 
				a.IsCompetitorProduct, a.InstallDate, a.Id, a.First_Call_Contr_End__c, a.FC_Status__c, 
				a.Estimated_EOL__c, a.Description, a.CreatedDate, a.CreatedById, a.ContactId, 
				a.CommCell__c, a.Cisco_Contract__c, a.AccountId ,a.Product_Info__r.BakBone__c,
				a.Product_Info__r.BakBone_Documentation__c,a.Product_Info__r.BakBone_Firmware__c
				from Asset a where id=:id];
		if(assets!=null && assets.size()>0){
			a=assets[0];
		}
		return null;
	}
	public TestMethod static void test(){
		Account acc=new Account();
		acc.Name='test';
		insert acc;
		Asset a=new Asset();
		a.AccountId=acc.id;
		a.Name='testAsset';
		insert a;
		AssetDetailPDF ass=new AssetDetailPDF();
		ass.id=acc.id;
		ass.init();
		ass.getA();
	}
}