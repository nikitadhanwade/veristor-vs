public class UploadCSVFile {
    
    public Static Boolean isSuccess=false;
    public Static List<String> csvColoumnName;
    Public static List<UploadWrapper> listofUploadWrapper{get;set;}
    
    @AuraEnabled
    public static List<UploadWrapper> readFile(blob attachment)
    {
        //List<string> priceList = new List<string>{'List Price','Disc. %','Net Price','Unit Price'};
        //List<string> nameList = new List<string>{'Qty','Prod #','Avail'};
        List<string> priceList = new List<string>();
        List<string> nameList = new List<string>();
        // Get all fields name datatype as text.
        List<Text_Column__mdt> textColumnList = getTextColumnRecord();
        // Get all fields name datatype as number,currency,percent.
        List<Price_Column__mdt> priceColumnList = getPriceColumnRecord();
        for(Text_Column__mdt textCol : textColumnList){
            nameList.add(textCol.label);
        }
        for(Price_Column__mdt priceCol : priceColumnList){
            priceList.add(priceCol.label);
        }
        csvColoumnName = new List<string>();
        integer idOfColoumn = 0 ;
        Integer rowsize=0;
        listofUploadWrapper = new List<UploadWrapper>();
        if(attachment!=null){
            List < String > fileData = new list<string>();
            String fileContent;
            map<String, List<String>> colNameVsValue = new map<String, List<String>>();
            List<Contact> contactList = new List<Contact>();
            fileContent = attachment.toString();
            fileData = fileContent.split('\n');
            csvColoumnName = fileData[0].split(',');
            String[] inputvalues = new String[]{};
                String value,colName;
            // Get all importColumn names.
            List<SBQQ__ImportColumn__c> listofImportColumn = [select Name from SBQQ__ImportColumn__c];
            List<String> listofImportCol = new List<String>();
            
            for(SBQQ__ImportColumn__c importCol : listofImportColumn){
                listofImportCol.add(importCol.Name);
            }
            System.debug('listofImportCol::--------------------------'+listofImportCol);
            for (Integer i=1,j=fileData.size();i<j;i++){
                inputvalues = new String[]{};
                    // get one by one rows of csv file
                    inputvalues = fileData[i].split(',');
                System.debug('inputvalues::--------------------------'+inputvalues);
                List <String> recordList =  getRecords(inputvalues);
                System.debug('recordList::--------------------------'+recordList);
                rowsize=recordList.size();
                for(integer l=0;l<rowsize;l++){
                    if(csvColoumnName.get(l) != null){
                        // get header values of column
                        colName = csvColoumnName.get(l);
                    }
                    colName= colName.trim();
                    system.debug('colName----------------------'+colName);
                    if(String.isNotBlank(recordList[l]) )
                    {
                        system.debug('value----------------------'+value);
                        system.debug('value----------------------'+recordList[l]);
                        // get actuall value of csv
                        value= recordList[l].length()>255 ? recordList[l].substring(0,254) : recordList[l];
                        system.debug('value---'+value);
                        system.debug('colNameVsValue---'+colNameVsValue);
                        // fill the map column name vs list of data
                        if(colNameVsValue.containsKey(colName)) {
                            List<String> colValues = colNameVsValue.get(colName);
                            colValues.add(value);
                            colNameVsValue.put(colName,colValues);    
                        } else {
                            colNameVsValue.put(colName, new List<String> {value});
                        }
                        system.debug(' if colNameVsValue---'+colNameVsValue);
                    }else{
                        // check if value is null then we add 0 for number
                        if(priceList.contains(colName))    {
                            if(colNameVsValue.containsKey(colName)) {
                                List<String> colValues = colNameVsValue.get(colName);
                                colValues.add('0');
                                colNameVsValue.put(colName,colValues);    
                            } else {
                                colNameVsValue.put(colName, new List<String> {'0'});
                            }
                            // check if value is null then we add 0 for text
                        }else if(nameList.contains(colName)){
                            if(colNameVsValue.containsKey(colName)) {
                                List<String> colValues = colNameVsValue.get(colName);
                                colValues.add('No Record');
                                colNameVsValue.put(colName,colValues);    
                            } else {
                                colNameVsValue.put(colName, new List<String> {'No Record'});
                            }
                        }
                        
                        
                        system.debug('else colNameVsValue---'+colNameVsValue);
                    }
                }
                
            }
            system.debug('csvColoumnName-------'+csvColoumnName);
            // fill the wrapper method
            for(String csvColoumnName : colNameVsValue.keySet()){
                List<String> coloumnData = colNameVsValue.get(csvColoumnName);
                if(listofImportCol.contains(csvColoumnName)){
                    listofUploadWrapper.add(new UploadWrapper('MAP',csvColoumnName,csvColoumnName,coloumnData,idOfColoumn));
                    idOfColoumn++;
                }else{
                    listofUploadWrapper.add(new UploadWrapper('MAP','UNMAPPED',csvColoumnName,coloumnData,idOfColoumn));
                    idOfColoumn ++;
                }
            }
            system.debug('colNameVsValue-------'+colNameVsValue);
            system.debug('listofUploadWrapper-------'+listofUploadWrapper);
        } 
        return listofUploadWrapper;
    }
    
    @AuraEnabled
    public static List<SBQQ__ImportColumn__c> getSfdcField(String sfdcFieldName,String importFormatName){
        
        System.debug('sfdcFieldName----------'+sfdcFieldName);
        System.debug('importFormatName----------'+importFormatName);
        Type idArrType = Type.forName('List<string>');
        List<string> sfdcFieldNameWrapperList= new List<string>();
        if(sfdcFieldName != null){
            
            sfdcFieldNameWrapperList = (List<string>) JSON.deserialize(sfdcFieldName, idArrType);
        }
        List<SBQQ__ImportColumn__c> listofImportColumn = [select Name from SBQQ__ImportColumn__c where Name NOT IN :sfdcFieldNameWrapperList AND SBQQ__ImportFormat__r.SBQQ__ImportFormatName__c =:importFormatName];
        System.debug('listofImportColumn----------'+listofImportColumn);
        return listofImportColumn;
    }
    public class UploadWrapper{
        @AuraEnabled
        String fieldName{get;set;}
        @AuraEnabled
        String SalesforceFieldName{get;set;}
        @AuraEnabled
        String MapName{get;set;}
        @AuraEnabled
        List<string> data{get;set;}
        @AuraEnabled
        integer idOfColoumn{get;set;}
        public UploadWrapper(String MapName,String SalesforceFieldName,String fieldName,List<String> data ,integer idOfColoumn){
            this.fieldName=fieldName;
            this.SalesforceFieldName=SalesforceFieldName;
            this.MapName=MapName;
            this.data= data;
            this.idOfColoumn =idOfColoumn;
        } 
    }
    
    @AuraEnabled
    public static Void saveRecords(String QtyJSONList,String ProdJSONList,String DescriptionJSONList,String ListPriceJSONList,String DiscJSONList,String UnitPriceJSONList,
                                   String NetPriceJSONList,String AvailJSONList,String ProductLineJSONList,String ProductClassJSONList,String recordId,Integer RecordCount,
                                   String createLineGroupCol,String importFormatName)
    {
        try{
            List<string> qtyJSONWrapperList = new List<string>();
            List<string> prodJSONWrapperList= new List<string>();
            List<string> descriptionJSONWrapperList = new List<string>();
            List<string> listPriceJSONWrapperList= new List<string>();
            List<string> discJSONWrapperList= new List<string>();
            List<string> netPriceJSONWrapperList= new List<string>();
            List<string> availJSONWrapperList= new List<string>();
            List<string> productLineJSONWrapperList= new List<string>();
            List<string> productClassJSONWrapperList= new List<string>();
            List<string> unitPriceJSONWrapperList= new List<string>();
            Map<String,String> quoteLineNameVsIdMap = new Map<String,String>();
            Map<String,Product2> productCodeVsOldProductMap = new Map<String,Product2>();
            Map<String,Product2> productCodeVsNewProductMap = new Map<String,Product2>();
            
            // call createList method to deserialize the json and store in List of string
            qtyJSONWrapperList = createList(QtyJSONList,RecordCount);     
            prodJSONWrapperList = createList(ProdJSONList,RecordCount); 
            descriptionJSONWrapperList = createList(DescriptionJSONList,RecordCount);
            listPriceJSONWrapperList = createList(ListPriceJSONList,RecordCount);
            discJSONWrapperList = createList(DiscJSONList,RecordCount);
            unitPriceJSONWrapperList = createList(UnitPriceJSONList,RecordCount); 
            netPriceJSONWrapperList = createList(NetPriceJSONList,RecordCount);
            availJSONWrapperList = createList(AvailJSONList,RecordCount);
            productLineJSONWrapperList = createList(ProductLineJSONList,RecordCount);
            productClassJSONWrapperList = createList(ProductClassJSONList,RecordCount);
            
            Set<String> productCodeSet = new Set<string>();
            List<QuoteLineWrapper  > quoteLineWrapperList = new List<QuoteLineWrapper >();
            List<Product2> productList = new List<Product2>();
            List<PricebookEntry> priceBookList = new List<PricebookEntry>();
            Database.SaveResult[] ProductResults;
            Database.SaveResult[] priceBookResults;
            Integer counts=1;
            System.debug('prodJSONWrapperList------'+prodJSONWrapperList);
            
            // call QuoteLineWrapper class to map specific list of data to specidic name
            for(integer i = 0 ; i < ProdJSONWrapperList.size();i++ ){
                QuoteLineWrapper quoteLineRecord = new QuoteLineWrapper(qtyJSONWrapperList[i],prodJSONWrapperList[i],
                                                                        descriptionJSONWrapperList[i],listPriceJSONWrapperList[i],
                                                                        discJSONWrapperList[i],unitPriceJSONWrapperList[i],
                                                                        netPriceJSONWrapperList[i],productClassJSONWrapperList[i]);
                quoteLineWrapperList.add(quoteLineRecord);
                productCodeSet.add(prodJSONWrapperList[i]);
            }
            System.debug('quoteLineWrapperList---'+quoteLineWrapperList);
            //get existing Product Info
            //This is map of Product Name vs Product 
            productCodeVsOldProductMap = getProductInfo(productCodeSet);
            System.debug('productCodeVsOldProductMap---'+productCodeVsOldProductMap);
            for(QuoteLineWrapper qouteLineWrappreObject : quoteLineWrapperList){
                // checking existing product and listprice is not 0
                boolean isOtherProduct = (!productCodeVsOldProductMap.containsKey(qouteLineWrappreObject.productName)) 
                    && qouteLineWrappreObject.listPrice != '0';
                
                Product2 prod = new Product2();
                if(isOtherProduct){
                    prod.Name=qouteLineWrappreObject.productName;
                    prod.Description=qouteLineWrappreObject.description;
                    prod.IsActive=true;
                    prod.ProductCode=qouteLineWrappreObject.productName;
                    prod.Manufacturer__c='Test';
                    prod.SBQQ__PriceEditable__c=true;
                    prod.SBQQ__AssetConversion__c='one per unit';
                    prod.SBQQ__PricingMethod__c='List';
                    prod.SBQQ__SubscriptionPricing__c='fixed price';
                    prod.SBQQ__SubscriptionTerm__c=36;
                    prod.SBQQ__SubscriptionType__c='renewable';
                    prod.SBQQ__ChargeType__c='One-Time'; 
                    productList.add(prod);
                }     
            }
            System.debug('productList---'+productList);
            if( productList != null && productList.size()>0){
                ProductResults  = Database.insert(productList);
                for(Product2 prod2 : productList){
                    if(prod2.Id!=null){
                        // map productcode vs product
                        productCodeVsNewProductMap.put(prod2.ProductCode,prod2);
                    }
                }
                System.debug('productCodeVsNewProductMap------'+productCodeVsNewProductMap);
                //get default PriceBook
                Pricebook2 priceBookObject = getStandaredPriceBook();
                //Assign inserted product to price book
                System.debug('priceBookObject---'+priceBookObject);
                for(string productCode : productCodeVsNewProductMap.keySet()){
                    PricebookEntry priceBookEntryObject = new PricebookEntry();
                    priceBookEntryObject.Pricebook2Id = priceBookObject.Id; 
                    Product2 productObject = productCodeVsNewProductMap.get(productCode);
                    priceBookEntryObject.Product2Id = productObject.Id;
                    priceBookEntryObject.UnitPrice = 0;
                    priceBookEntryObject.isActive=true;
                    priceBookList.add(priceBookEntryObject);
                }
                if(priceBookList.size()>0){
                    priceBookResults  = Database.insert(priceBookList);
                }
            } 
            System.debug('priceBookResults------'+priceBookResults);
            List<SBQQ__QuoteLineGroup__c> quoteLineGroupList = new List<SBQQ__QuoteLineGroup__c>();
            Set<String> setProductClass = new Set<String>();
            List<String> listProductClass = new List<String>();
            List<String> selectedLineGroup = new List<String>();
            System.debug('importFormatName------'+importFormatName);
            // call createLineGroupList method to get list of data which match passed value from Line Group Column
        if(!String.isEmpty(createLineGroupCol)){
            selectedLineGroup = createLineGroupList(createLineGroupCol,importFormatName,qtyJSONWrapperList,prodJSONWrapperList,descriptionJSONWrapperList,listPriceJSONWrapperList,
                                                    discJSONWrapperList,unitPriceJSONWrapperList,netPriceJSONWrapperList,availJSONWrapperList,productLineJSONWrapperList,productClassJSONWrapperList);
        }
            List<SelectQuoteLineGroupWrapper> selectQuoteLineGroupList = new List<SelectQuoteLineGroupWrapper>();
            if(selectedLineGroup.size()>0 && selectedLineGroup != null){
                for(integer i = 0 ; i < ProdJSONWrapperList.size();i++ ){
                    // call SelectQuoteLineGroupWrapper class to map specific list of data to specific name
                    SelectQuoteLineGroupWrapper selectQuoteLineGroupRecord = new SelectQuoteLineGroupWrapper(listPriceJSONWrapperList[i],selectedLineGroup[i]);
                    selectQuoteLineGroupList.add(selectQuoteLineGroupRecord);
                }
            }
            if(selectedLineGroup.size()>0 && selectedLineGroup != null){
                for(SelectQuoteLineGroupWrapper qouteLineWrappreObject : selectQuoteLineGroupList)
                {  
                    // checking list price not equal to 0 and add unique value in set
                    if(qouteLineWrappreObject.listPrice != '0'){
                        setProductClass.add(qouteLineWrappreObject.selectQuoteLineGroup.replace('\r',''));    
                    }
                }
            }
            listProductClass = new List<String>(setProductClass);
            System.debug('listProductClass-----'+listProductClass);
            // checking Group Line Items is checked on quote
            List<SBQQ__Quote__c> sbqqQuoteList =[Select SBQQ__LineItemsGrouped__c from SBQQ__Quote__c where id =:recordId];
            System.debug('recordId-----'+recordId);
            System.debug('sbqqQuoteList-----'+sbqqQuoteList);
            Integer count=1;
            
            updateQuoteLine(recordId);
            
            for(SBQQ__Quote__c quote : sbqqQuoteList){
                
                    for(integer i =0 ;i<listProductClass.size();i++){
                        SBQQ__QuoteLineGroup__c qlg = new SBQQ__QuoteLineGroup__c();
                        qlg.Name = listProductClass[i];
                        qlg.SBQQ__Quote__c=recordId;
                        qlg.SBQQ__Number__c = count;
                        qlg.SBQQ__ListTotal__c = 0;
                        qlg.SBQQ__CustomerTotal__c=0;
                        qlg.SBQQ__NetTotal__c=0;
                        count++;
                        quoteLineGroupList.add(qlg);
                    
                }
            }
            System.debug('quoteLineGroupList-----'+quoteLineGroupList);
            Database.SaveResult[] results;
            List<Database.SaveResult> failedAccList = new List<Database.SaveResult>();
            if(quoteLineGroupList.size()>0){
                results  = Database.insert(quoteLineGroupList);
                for(Database.SaveResult ds :results){
                    if (ds.isSuccess() == false)
                        failedAccList.add(ds);    
                }
            }
            System.debug('results------'+results);
            List<SBQQ__QuoteLine__c> quoteLinelist = new List<SBQQ__QuoteLine__c>();
            for(SBQQ__QuoteLineGroup__c listQLG :  quoteLineGroupList){
                if(listQLG.Id!=null){
                    // map name vs id
                    quoteLineNameVsIdMap.put(listQLG.Name,listQLG.Id);
                }
            }
            List<SBQQ__QuoteLine__c> qouteLineRecordList = new List<SBQQ__QuoteLine__c>();
            for(QuoteLineWrapper qouteLineWrappreObject : quoteLineWrapperList)
            {  
                if(qouteLineWrappreObject.listPrice != '0'){
                    SBQQ__QuoteLine__c quoteLineObject = new SBQQ__QuoteLine__c();
                    quoteLineObject.SBQQ__Quantity__c = decimal.valueOf(qouteLineWrappreObject.quantity);
                    quoteLineObject.Prod__c = qouteLineWrappreObject.productName;
                    quoteLineObject.Part_Description__c = qouteLineWrappreObject.description;
                    String unitPrice=qouteLineWrappreObject.unitPrice.replace('$','');
                    unitPrice = unitPrice.replace(',','');
                    quoteLineObject.SBQQ__ListPrice__c = decimal.valueOf(unitPrice.trim());
                    string listPrice= qouteLineWrappreObject.listPrice.replace('$','');
                    listPrice = listPrice.replace(',','');
                    System.debug('listPrice-----------'+listPrice);
                    quoteLineObject.List_Price_Csv__c = decimal.valueOf(listPrice.trim());            
                    string netPrice= qouteLineWrappreObject.netPrice.replace('$','');
                    netPrice = netPrice.replace(',','');
                    quoteLineObject.Net_Price__c = decimal.valueOf(netPrice.trim());
                    quoteLineObject.Product_Class__c = qouteLineWrappreObject.productClass;
                    quoteLineObject.Disc_Csv__c = decimal.valueOf(qouteLineWrappreObject.discount);
                    quoteLineObject.SBQQ__Quote__c=recordId;
                    
                    if(selectedLineGroup != null){
                        if(quoteLineNameVsIdMap.containsKey(qouteLineWrappreObject.productClass.trim())){
                            quoteLineObject.SBQQ__Group__c = quoteLineNameVsIdMap.get(qouteLineWrappreObject.productClass.trim());
                        }else if(quoteLineNameVsIdMap.containsKey(qouteLineWrappreObject.discount.trim())){
                            quoteLineObject.SBQQ__Group__c = quoteLineNameVsIdMap.get(qouteLineWrappreObject.discount.trim());
                        }else if(quoteLineNameVsIdMap.containsKey(qouteLineWrappreObject.productName.trim())){
                            quoteLineObject.SBQQ__Group__c = quoteLineNameVsIdMap.get(qouteLineWrappreObject.productName.trim());
                        }else if(quoteLineNameVsIdMap.containsKey(qouteLineWrappreObject.quantity.trim())){
                            quoteLineObject.SBQQ__Group__c = quoteLineNameVsIdMap.get(qouteLineWrappreObject.quantity.trim());
                        }else if(quoteLineNameVsIdMap.containsKey(qouteLineWrappreObject.description.trim())){
                            quoteLineObject.SBQQ__Group__c = quoteLineNameVsIdMap.get(qouteLineWrappreObject.description.trim());
                        }else if(quoteLineNameVsIdMap.containsKey(qouteLineWrappreObject.unitPrice.trim())){
                            quoteLineObject.SBQQ__Group__c = quoteLineNameVsIdMap.get(qouteLineWrappreObject.unitPrice.trim());
                        }else if(quoteLineNameVsIdMap.containsKey(qouteLineWrappreObject.listPrice.trim())){
                            quoteLineObject.SBQQ__Group__c = quoteLineNameVsIdMap.get(qouteLineWrappreObject.listPrice.trim());
                        } 
                    }
                    
                    if(qouteLineWrappreObject.productName != null){
                        if(productCodeVsOldProductMap.containsKey(qouteLineWrappreObject.productName.trim())){
                            Product2 productObject = productCodeVsOldProductMap.get(qouteLineWrappreObject.productName.trim());
                            quoteLineObject.SBQQ__Product__c= productObject.Id;
                            quoteLineObject.SBQQ__SubscriptionPricing__c = productObject.SBQQ__SubscriptionPricing__c;
                            quoteLineObject.SBQQ__DefaultSubscriptionTerm__c = productObject.SBQQ__SubscriptionTerm__c;
                            quoteLineObject.Subscription_Type__c = productObject.SBQQ__SubscriptionType__c;
                            quoteLineObject.SBQQ__PriceEditable__c=productObject.SBQQ__PriceEditable__c;
                            quoteLineObject.SBQQ__ChargeType__c =productObject.SBQQ__ChargeType__c;
                            quoteLineObject.SBQQ__PricingMethod__c =productObject.SBQQ__PricingMethod__c;
                        }else{
                            Product2 productObject = productCodeVsNewProductMap.get(qouteLineWrappreObject.productName.trim());
                            quoteLineObject.SBQQ__SubscriptionPricing__c = productObject.SBQQ__SubscriptionPricing__c;
                            quoteLineObject.SBQQ__DefaultSubscriptionTerm__c = productObject.SBQQ__SubscriptionTerm__c;
                            quoteLineObject.Subscription_Type__c = productObject.SBQQ__SubscriptionType__c;
                            quoteLineObject.SBQQ__PriceEditable__c=productObject.SBQQ__PriceEditable__c;
                            quoteLineObject.SBQQ__ChargeType__c =productObject.SBQQ__ChargeType__c;
                            quoteLineObject.SBQQ__PricingMethod__c =productObject.SBQQ__PricingMethod__c;
                            quoteLineObject.SBQQ__Product__c= productObject.id;
                        }
                    }
                    quoteLineObject.SBQQ__Existing__c =false;
                    quoteLineObject.SBQQ__Renewal__c=true;
                    quoteLineObject.SBQQ__PriorQuantity__c = 0;
                    quoteLineObject.SBQQ__CustomerPrice__c = 0;
                    quoteLineObject.SBQQ__NetPrice__c = 0;   
                    quoteLineObject.SBQQ__Number__c = count;
                    count++;
                    qouteLineRecordList.add(quoteLineObject);
                }  
            }
            if(qouteLineRecordList.size()>0){
                insert qouteLineRecordList;
            }
            
            if(!String.isEmpty(createLineGroupCol)){
                SBQQ__Quote__c quote = new SBQQ__Quote__c();
                quote.SBQQ__LineItemsGrouped__c = true;
                quote.Id=recordId;
                update quote;
            }else{
                SBQQ__Quote__c quote = new SBQQ__Quote__c();
                quote.SBQQ__LineItemsGrouped__c = false;
                quote.Id=recordId;
                update quote;
            }
        }catch(Exception e){
            
            throw new AuraHandledException('Error Exception happend'+e.getMessage());           
            
        }
    }
    
    public static void updateQuoteLine(String RecordId){
        List<SBQQ__QuoteLine__c> quoteLineList = [select id from SBQQ__QuoteLine__c where SBQQ__Group__c=null and SBQQ__Quote__c=: recordId];
        SBQQ__QuoteLineGroup__c qlg = new SBQQ__QuoteLineGroup__c();
        if(quoteLineList != null && quoteLineList.size()>0){
            Integer count=1;
            
            qlg.Name = 'Ungroup Lines';
            qlg.SBQQ__Quote__c=recordId;
            qlg.SBQQ__Number__c = count;
            qlg.SBQQ__ListTotal__c = 0;
            qlg.SBQQ__CustomerTotal__c=0;
            qlg.SBQQ__NetTotal__c=0;
            count++;
            insert qlg;
        }
        
        List<SBQQ__QuoteLine__c> updateQuoteLineList = new List<SBQQ__QuoteLine__c>();
        for(SBQQ__QuoteLine__c quoteLineRecord : quoteLineList){
            SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c();
            quoteLine.SBQQ__Group__c = qlg.id;
            quoteLine.id = quoteLineRecord.id;
            updateQuoteLineList.add(quoteLine);
        }
      if(updateQuoteLineList.size()>0){
          update updateQuoteLineList;
      }
        
    }
    // Remove double quotes
    public static List<String> getRecords (List<String> recordsString){
        system.debug('Data to Parse : ' +recordsString );
        string recordString = string.join(recordsString,',');
        List<string> recordList = new List<String>();
        String csvLine = recordString;
        system.debug('csvLine : ' +csvLine );
        String prevLine = csvLine;
        Integer startIndex;
        Integer endIndex;
        while(csvLine.indexOf('"') > -1){
            if(startIndex == null){
                startIndex = csvLine.indexOf('"');
                csvLine = csvLine.substring(0, startIndex) + ':quotes:' + csvLine.substring(startIndex+1, csvLine.length());
            }else{
                if(endIndex == null){
                    endIndex = csvLine.indexOf('"');
                    csvLine = csvLine.substring(0, endIndex) + ':quotes:' + csvLine.substring(endIndex+1, csvLine.length());
                }
            }
            if(startIndex != null && endIndex != null){
                String sub = csvLine.substring(startIndex, endIndex);
                sub = sub.replaceAll(',', ':comma:');
                csvLine = csvLine.substring(0, startIndex) + sub + csvLine.substring(endIndex, csvLine.length());
                startIndex = null;
                endIndex = null;
            }
        }
        System.debug('prevLine:::'+prevLine);
        System.debug('csvLine:::'+csvLine);
        for(String column : csvLine.split(',')){
            column = column.replaceAll(':quotes:', '').replaceAll(':comma:', ',');
            if(String.isBlank(column)){
                column = null;
            }
            System.debug('column::'+column);
            recordList.add(column);
        }
        System.debug('column::'+recordList);
        return recordList;
    }
    
    public static List<string> createList(string jsonData,Integer recordCount){
        Type idArrType = Type.forName('List<string>');
        if(jsonData != null){
            return (List<string>) JSON.deserialize(jsonData, idArrType);
        }else{
            List<String> emptyList = new List<String>(); 
            for(Integer i=0; i<recordCount; i++){
                emptyList.add('0');
            }
            return emptyList;
        }
    }
    
    public static List<string> createLineGroupList(string importColumnName,String importFormatName,List<String> quantityList,List<String> productList,List<String> descriptionList,List<String> listPriceList,
                                                   List<String> discList,List<String> unitPriceList,List<String> netPriceList,List<String> availList,List<String> productLineList,List<String> productClassList)
    {
        Set<String> productClassSet = new Set<String>(); 
       
            if(importColumnName.equalsIgnoreCase('Qty') || importColumnName.equalsIgnoreCase('Quantity')){
                return quantityList;
            }else if(importColumnName.equalsIgnoreCase('Prod #') || importColumnName.equalsIgnoreCase('Product Code')){
                return productList;         
            }else if(importColumnName.equalsIgnoreCase('Description')){
                return descriptionList;
            }else if(importColumnName.equalsIgnoreCase('List Price')){
                return listPriceList;
            }else if(importColumnName.equalsIgnoreCase('Disc. %')){
                return discList;
            }else if(importColumnName.equalsIgnoreCase('Unit Price')){
                return unitPriceList;
            }else if(importColumnName.equalsIgnoreCase('Net Price')){
                return netPriceList;
            }else if(importColumnName.equalsIgnoreCase('Product Line')){
                return productLineList;
            }else if(importColumnName.equalsIgnoreCase('ProductType') || importColumnName.equalsIgnoreCase('Product Class')){
                return productClassList;
            }else{
                return availList; 
            }
    }
    
    public static Map<string,Product2> getProductInfo(set<String> productCode)  
    {
        Map<String,Id> productCodeVsProductIdMap = new Map<String,Id>();
        Map<String,Product2> productCodeVsOldProductMap = new Map<String,Product2>();
        List<Product2> productList = [SELECT Id,
                                      Name,
                                      productCode ,
                                      SBQQ__SubscriptionPricing__c,
                                      SBQQ__SubscriptionTerm__c,
                                      SBQQ__SubscriptionType__c,
                                      SBQQ__PriceEditable__c,
                                      SBQQ__AssetConversion__c,
                                      SBQQ__ChargeType__c,
                                      SBQQ__PricingMethod__c
                                      from Product2
                                      where productCode in : productCode
                                     ];
        if(productList != null && productList.size()>0){
            for(Product2 productRecord : productList){
                productCodeVsOldProductMap.put(productRecord.productCode,productRecord);
            }
        }
        return productCodeVsOldProductMap;
    }
    public static Pricebook2 getStandaredPriceBook(){
        Pricebook2  priceBookObject   = [select Id,
                                         Name, 
                                         IsActive 
                                         from PriceBook2 
                                         where IsStandard=True LIMIT 1];
        return priceBookObject;
    }
    
    // get import format name
    @AuraEnabled
    public static LIST<SBQQ__ImportFormat__c> getImportFileFormats(){
        List<SBQQ__ImportFormat__c> ColomnNames = [select SBQQ__ImportFormatName__c from SBQQ__ImportFormat__c];
        System.debug('In fun-------');
        return ColomnNames;
    }
    
    // get import column name
    @AuraEnabled
    public static List<SBQQ__ImportColumn__c> getMandatoryFieldsOfImportColumns(){
        Map<String,List<String>> nameVsObjectMap = new Map<String,List<String>>();
        List<SBQQ__ImportColumn__c> importFormatList = [select Id,Name,SBQQ__ImportFormat__c,SBQQ__ImportFormat__r.SBQQ__ImportFormatName__c,IsCheck__c from SBQQ__ImportColumn__c];
        if(importFormatList != null){
            return importFormatList; 
        }
        return null;
    }
    
    // get import column for particular import format
    @AuraEnabled
    public static LIST<SBQQ__ImportColumn__c> getSelectImportFormatRecord(String selectedImportFormat){
        System.debug('selectedImportFormat---------'+selectedImportFormat);
        List<SBQQ__ImportColumn__c> listofImportColumnData = [select Name from SBQQ__ImportColumn__c where SBQQ__ImportFormat__r.SBQQ__ImportFormatName__c =: selectedImportFormat];
        System.debug('listofImportColumnData------'+listofImportColumnData);
        return listofImportColumnData;
    }
    
    // get import column for particular import format and can group is true
    @AuraEnabled
    public static LIST<SBQQ__ImportColumn__c> getRecordForLineGroup(String selectedImportFormat){
        System.debug('selectedImportFormat---------'+selectedImportFormat);
        List<SBQQ__ImportColumn__c> listofImportColumnData = [select Name from SBQQ__ImportColumn__c where SBQQ__ImportFormat__r.SBQQ__ImportFormatName__c =: selectedImportFormat AND Can_Group__c = True];
        System.debug('listofImportColumnData------'+listofImportColumnData);
        return listofImportColumnData;
    }
    
    // get label from Text_Column__mdt custom metadata
    Public static List<Text_Column__mdt> getTextColumnRecord(){
        List<Text_Column__mdt> textColumnList = [select label from Text_Column__mdt];
        return textColumnList;
    }
    
    // get label from Price_Column__mdt custom metadata
    Public static List<Price_Column__mdt> getPriceColumnRecord(){
        List<Price_Column__mdt> priceColumnList = [select label from Price_Column__mdt];
        return priceColumnList;
    }
}