public class QuoteLineWrapper {

    public string quantity;
    public string productName;
    public string description;
    public string listPrice;
    public string discount;
    public string unitPrice;
    public string netPrice;
    //public string Avail;
    //public string productLine;
    public string productClass;
    
    
    public QuoteLineWrapper(string quantity,string productName,string description,string listPrice,string discount,string unitPrice,string netPrice,string productClass){
        this.quantity = quantity;
        this.productName = productName;
        this.description = description;
        this.listPrice = listPrice;
        this.discount = discount;
        this.unitPrice = unitPrice;
        this.netPrice = netPrice;
        //this.Avail = Avail;
        //this.productLine = productLine;
        this.productClass = productClass;
               
    }
}