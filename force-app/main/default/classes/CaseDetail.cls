public class CaseDetail {
	public Case c=new Case();
	public String id=ApexPages.currentPage().getParameters().get('id');
	
	//public EmailMessage[] mess{get;set;}
	
	public Case getC(){
		return this.c;
	}
	public CaseDetail(){
		System.debug('-------------constuctor--------------------');
	}
	
	public CaseComment[] ccs{get;set;}
	public Attachment[] atts{get;set;}
	public String comm{get;set;}
	public String fileName{get;set;}
	public blob filebody{get;set;}
	
	public pageReference init(){
		ccs=new CaseComment[]{};
		atts=new Attachment[]{};
		//mess=new EmailMessage[]{};
		System.debug('-------------init--------------------');
		List<Case> cases=[select c.Vendor__c, c.VMware_Customer_Number__c, c.Type,
			c.Supported_First_Call__c, c.SuppliedPhone, c.SuppliedName, c.SuppliedEmail, 
			c.SuppliedCompany, c.Subject, c.Status, c.Reason, c.Product_Serial_Number__c, 
			c.Product_Model__c, c.Priority, c.OwnerId,c.Owner.Name,c.Contact.Name,c.Contact.Phone,c.Contact.Email,
			c.Id,c.Firmware_Level__c, c.Description, c.CreatedDate, c.ContactId, c.Account.Name,c.Asset.Name,
			c.Asset.Manufacturer_Name__c,c.Asset.SerialNumber,
			(Select Id, ParentId, IsPublished, CommentBody,CreatedDate From CaseComments where IsPublished=true),
			(Select TextBody, HtmlBody, Headers,Subject, FromName, FromAddress, ToAddress, CcAddress, BccAddress, Status, MessageDate From EmailMessages),
			(Select Id, Name From Attachments),
			c.ClosedDate, c.CaseNumber, c.AssetId, c.AccountId 
			from Case c where id=:id 
				order by CreatedDate desc limit 999];
		if(cases!=null && cases.size()>0){
			c=cases[0];
			/*
			for(EmailMessage me:c.EmailMessages){
				if(me.TextBody!=null && me.TextBody.length()>255){
					me.TextBody=me.TextBody.substring(0,255);
				}
				if(me.HtmlBody!=null && me.HtmlBody.length()>255){
					me.HtmlBody=me.HtmlBody.substring(0,255);
				}
				mess.add(me);
			}
			*/
			ccs.addAll(c.CaseComments);
			atts.addAll(c.Attachments);
		}
		return null;
	}
	public void addComm(){
		if(comm!=null && comm!=''){
			CaseComment com=new CaseComment();
			com.ParentId=c.id;
			com.CommentBody=comm;
			com.IsPublished=true;
			insert com;
			ccs.add(com);
			comm=null;
		}
	}
	public void addAtt(){
		System.debug('----addAtt---------'+fileName);
		if(fileName!=null && fileName!=''){
			Attachment att=new Attachment();
			att.Name=fileName;
			att.ParentId=c.id;
			att.Body=filebody;
			insert att;
			atts.add(att);
		}
	}
	public TestMethod static void test(){
		Account acc=new Account();
		acc.Name='test';
		insert acc;
		Case c=new Case();
		c.AccountId=acc.id;
		insert c;
		
		CaseDetail a=new CaseDetail();
		a.id=c.Id;
		a.init();
		a.getC();
	}
}