public class PrintSelectedAssets {
	public String ids=ApexPages.currentPage().getParameters().get('ids');
	public String accName{get;set;}
	public List<Asset> assets=new List<Asset>();
	public List<Asset> getAssets(){
		return this.assets;
	}
	public PrintSelectedAssets(){
		
	}
	public PageReference init(){
		List<User> us=[select id,AccountId from User where id=:userinfo.getUserId()];
		if(us.size()>0){
			Account[] ass=[select id,Name from Account where id=:us[0].AccountId limit 1];
			if(ass.size()>0){
				accName=ass[0].Name;
			}
			
		}
		if(ids!=null && ids!=''){
			String[] s=ids.split(';');
			assets=[select id,Name,Status,Manufacturer_Name__c,
				Service_Tag__c,SerialNumber,FC_Status__c,Man_Maint_End__c,First_Call_Contr_End__c
			from Asset where id in:s order by name desc limit 1000];
		}
		return null;
	}
	public TestMethod static void test(){
		List<Asset> assets=[select id from Asset limit 3];
		String ids='';
		if(assets!=null && assets.size()>0){
			for(Asset a:assets){
				ids+=a.id+';';
			}
		}
		PrintSelectedAssets p=new PrintSelectedAssets();
		p.ids=ids;
		p.init();
		p.getAssets();
	}
}