({
    doInit : function(component,event,helper) {
        debugger;
        helper.getFieldName(component, event, helper);
        helper.getMandatoryFields(component, event, helper);
    },
    save : function(component,event,helper) {
        helper.handleSave(component, event, helper);
    },
    
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.uploadCase", false);
        component.set("v.showFieldPopoUp", false);
        component.set("v.ErrorMessageBlock", false);
        component.set("v.DuplicateMessageBlock", false);
        component.set("v.SuccessMessageBlock", false);
    },
    mapSalesforceFields : function(component,event,helper){
        helper.handleMapSalesforceFields(component, event, helper);
    },
    selectField : function(component,event,helper){
        helper.handleSelectField(component, event, helper);
    },
    handleRowAction: function (cmp, event, helper) {
        debugger;
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'view_details':
                helper.showRowDetails(row,cmp,event,helper);
                break;
            case 'edit_status':
                helper.editRowStatus(cmp, row, action);
                break;
            default:
                helper.showRowDetails(row,cmp,event,helper);
                break;
        }
    },
    createRecord:  function (cmp, event, helper) {
        debugger;
        helper.createRecordHelper(cmp, event, helper);
    },
    onRecordSizeChange : function(component,event,helper){
        debugger;
        helper.onRecordSizeChangeHelper(component, event, helper);
        helper.onRecordChangeHelper(component, event, helper);
    },
    onChangeLineGroup : function(component,event,helper){
        debugger;
        var recordToDisply = component.find("lineGroupCol").get("v.value");
        component.set("v.lineGroupColName",recordToDisply);
    }, 
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        debugger;
        component.set("v.spinner", true); 
    },
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    }
})